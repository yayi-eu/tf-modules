provider "aws" {
  region     = var.aws_region
}
resource "aws_organizations_organizational_unit" "ou" {
  name      = var.ou_name
  parent_id = var.master_ou_id
}
resource "aws_organizations_account" "acount" {
  parent_id = aws_organizations_organizational_unit.ou.id
  name  = var.ou_name
  email = var.ou_email

}

resource "aws_organizations_policy_attachment"  "polcies_attachment"{
  policy_id = var.ou_policy_id
  target_id = aws_organizations_account.acount.id
}
