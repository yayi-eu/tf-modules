variable "aws_region" {
  description = "AWS region"
}
variable "ou_name" {
  description = "The organizations unit Name"
}
variable "master_ou_id" {
  description = "master  organizations unit id"
}
variable "ou_email" {
  description = "The email of the organizations unit"
}
variable "ou_policy_id" {
  description = "The organizations unit policy"
}
