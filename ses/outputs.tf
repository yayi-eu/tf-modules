output "ses_verification_token" {
  value = join("", aws_ses_domain_identity.domain.*.verification_token)
}
