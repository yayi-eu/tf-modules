provider "aws" {
  region     = var.aws_region
  assume_role {
    role_arn = var.provider_env_roles
  }
}
provider "aws" {
  alias = "route53"
  region     = var.aws_region
}
