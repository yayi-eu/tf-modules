resource "aws_route53_zone" "domain" {
  name = var.domain
}

resource "aws_route53_record" "domain_mx_google" {
  count = var.is_mx_google ? 1 : 0
  name = aws_route53_zone.domain.name
  type = "MX"
  zone_id = aws_route53_zone.domain.id
  ttl = "300"
  records = [
    "0 aspmx.l.google.com.",
    "5 alt2.aspmx.l.google.com.",
    "10 aspmx2.googlemail.com.",
    "10 aspmx3.googlemail.com."
  ]
}
resource "aws_route53_record" "verify_domain" {
  count = var.is_mx_google ? 1 : 0
  name = aws_route53_zone.domain.name
  type = "TXT"
  ttl = 60
  zone_id = aws_route53_zone.domain.id
  records = [var.google_verification_record]
}
resource "aws_route53_record" "amazonses_verification_record" {
  zone_id = aws_route53_zone.domain.id
  name    = "_amazonses.${var.domain}"
  type    = "TXT"
  ttl     = "600"
  records = var.amazonses_verification_records
}
