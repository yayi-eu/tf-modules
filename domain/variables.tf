variable "aws_region" {
  description = "AWS region"
}
variable "domain" {
  description = "domain"
}
variable "is_mx_google" {
  description = "equals true if we use google as mail provider"
  default = false
}
variable "google_verification_record" {
  description = "the google verification token"
  default = "depends on is_mx_google"
}
variable "amazonses_verification_records" {
  description = "the amazon ses verfication records for all organizations"
}
