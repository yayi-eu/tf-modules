variable "runner_name" {
  description = "runner_name"
}
variable "runner_token" {
  description = "runner_token"
}
variable "runner_volumes" {
  description = ""
}
variable "gitlab_runner_image" {
  description = ""
}
variable "runner_base_image" {
  description = ""
}
variable "runner_m2_path" {
  description = "atn_runner_m2_path"
}
variable "runner_data_path" {
  description = "runner_data_path"
}
variable "runner_docker_container_name" {
  description = ""
}

