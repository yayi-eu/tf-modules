
data "docker_registry_image" "gitlab_runner" {
  name = var.gitlab_runner_image
}
data "docker_registry_image" "runner_base_image" {
  name = var.runner_base_image
}
