resource "docker_image" "runner" {
  name = data.docker_registry_image.gitlab_runner.name
  pull_triggers = [data.docker_registry_image.gitlab_runner.sha256_digest]
  keep_locally = true
}


resource "docker_container" "yayi_runner" {
  image = docker_image.runner.latest
  name = var.runner_docker_container_name
  restart = "always"
  volumes {
    host_path = "/var/run/docker.sock"
    container_path = "/var/run/docker.sock"
    read_only = false
  }
  volumes {
    host_path = var.runner_m2_path
    container_path = "/var/lib/gitlab-runner/.m2"
    read_only = false
  }
  volumes {
    host_path = var.runner_m2_path
    container_path = "/var/lib/gitlab-runner/.m2"
    read_only = false
  }
  volumes {
    host_path = var.runner_data_path
    container_path = "/etc/gitlab-runner"
  }

  network_mode = "host"

}

resource "null_resource" "register_runner" {
  depends_on = [
    docker_container.yayi_runner
  ]

  provisioner "local-exec" {
    command =<<EOT
        sleep 5 && docker run --rm -v ${var.runner_data_path}:/etc/gitlab-runner gitlab/gitlab-runner register      --non-interactive     --executor "docker"   --docker-image ${data.docker_registry_image.runner_base_image.name}      --url "https://gitlab.com/"      --registration-token ${var.runner_token}    --description ${var.runner_name}   --tag-list "docker,aws"    --run-untagged="true"     --locked="false"   --access-level="not_protected"
EOT
  }
}
resource "null_resource" "restart_runner" {
  depends_on = [
    docker_container.yayi_runner,null_resource.register_runner
  ]
  triggers = {
    always = uuid()
  }
  provisioner "local-exec" {
    command ="docker restart ${docker_container.yayi_runner.name}"
  }
}
